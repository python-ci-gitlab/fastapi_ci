import asyncio
from contextlib import asynccontextmanager
from typing import AsyncGenerator

import pytest_asyncio
from httpx import AsyncClient

from sqlalchemy.ext.asyncio import (
    AsyncSession,
    async_sessionmaker,
    create_async_engine
)

from ..src.main import app
from ..src.database import Base, get_db
from ..src.models import Recipe, Ingredient


# Create a test database
test_db = "sqlite+aiosqlite:////app/test.db"
engine = create_async_engine(test_db, future=True, echo=True)
async_session = async_sessionmaker(
    engine, expire_on_commit=False, class_=AsyncSession
)


@asynccontextmanager
async def override_get_async_session() -> AsyncGenerator[AsyncSession, None]:
    async with async_session() as session:
        yield session


app.dependency_overrides[get_db] = override_get_async_session


# Fixture to create tables in test database
@pytest_asyncio.fixture(autouse=True, scope="session")
async def init_db():
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)
    yield
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.drop_all)


@pytest_asyncio.fixture(scope="session")
def event_loop(request):
    """
    Override the scope of visibility on the session
    of the standard event_loop fixture
    """
    loop = asyncio.get_event_loop_policy().new_event_loop()
    yield loop
    loop.close()


# Fixture for HTTP client for requests
@pytest_asyncio.fixture(scope="session")
async def client() -> AsyncGenerator[AsyncClient, None]:
    async with AsyncClient(app=app, base_url="http://test") as ac:
        async with override_get_async_session() as session:
            yield ac


# Fixture to create a test recipe
@pytest_asyncio.fixture(scope="function")
async def recipe():
    async with override_get_async_session() as session:
        _recipe = Recipe(
            dish_name="Test recipe",
            cooking_time=30,
            description="Test description",
            views=0,
            ingredients=[
                Ingredient(
                    name="Картофель",
                    quantity=1,
                    unit="кг",
                    description="Картофель"
                ),
                Ingredient(
                    name="капуста",
                    quantity=1,
                    unit="кг",
                    description="капуста"
                )
            ]
        )
        session.add(_recipe)
        await session.commit()
        yield _recipe
        await session.delete(_recipe)
        await session.commit()
