import pytest

from ..src.models import Ingredient


@pytest.mark.asyncio
async def test_get_all_recipes(client, recipe):
    yield recipe
    response = await client.get("/recipes")
    assert response.status_code == 200
    assert isinstance(response.json(), list)


@pytest.mark.asyncio
async def test_get_recipe(client, recipe):
    yield recipe

    response = await client.get(f"/recipes/{recipe.id}")
    if response.status_code == 200:
        assert 'dish_name' in response.json()
        assert 'cooking_time' in response.json()
        assert 'ingredients' in response.json()
        assert 'description' in response.json()
    else:
        assert response.status_code == 404


@pytest.mark.asyncio
async def test_create_recipes(client, recipe):
    yield recipe

    data = [{
        "dish_name": "Тестовый рецепт",
        "cooking_time": 30,
        "description": "Описание тестового рецепта",
        "views": 0,
        "ingredients": [
            Ingredient(
                name="Картофель",
                quantity=1,
                unit="кг",
                description="Картофель"
            ),
            Ingredient(
                name="капуста",
                quantity=1,
                unit="кг",
                description="капуста"
            )
        ]
    }]

    response = await client.post('/recipes/', json=data)

    assert response.status_code == 201

    created_recipe = response.json()
    assert created_recipe["dish_name"] == "Тестовый рецепт"
    assert created_recipe["cooking_time"] == 30
