from typing import Any, List

import uvicorn
from fastapi import FastAPI, HTTPException, Path
from sqlalchemy.future import select
from sqlalchemy.orm import joinedload, subqueryload

from .database import Base, engine, async_session
from .models import Ingredient, Recipe
from .schemas import BaseRecipe, Recipes

app = FastAPI()


@app.on_event("startup")
async def startup():
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)


@app.on_event("shutdown")
async def shutdown():
    await async_session.close()
    await engine.dispose()


@app.get("/recipes", response_model=List[Recipes])
async def get_all_recipes():
    async with async_session() as session:
        result = await session.execute(
            select(Recipe)
            .options(subqueryload(Recipe.ingredients))
            .order_by(Recipe.views.desc(), Recipe.cooking_time)
        )
    return result.scalars().all()


@app.get("/recipes/{idx}", response_model=BaseRecipe)
async def get_recipe(idx: Any = None):
    if idx is None:
        idx = Path(
            ...,
            title="Id to display detailed information about the recipe",
        )
    async with async_session() as session:
        result = await session.execute(
            select(Recipe).where(Recipe.id == idx).options(joinedload(Recipe.ingredients))
        )
        recipe: Any = result.scalar()
        if recipe is None:
            raise HTTPException(status_code=404, detail="Recipe not found")
        else:
            recipe.views += 1
            session.add(recipe)
            await session.commit()
            return recipe


@app.post("/recipes/", response_model=List[Recipes])
async def create_recipes(_recipes: List[BaseRecipe]) -> List[Recipe]:
    new_recipes: List[Recipe] = [
        Recipe(
            ingredients=[i for i in recipe.ingredients],
            views=0,
            dish_name=recipe.dish_name,
            cooking_time=recipe.cooking_time,
            description=recipe.description
        )
        for recipe in _recipes
    ]
    
    async with async_session() as session:
        session.add_all(new_recipes)
        await session.commit()

    return new_recipes


recipes = [
    {
        "dish_name": "Борщ",
        "cooking_time": 90,
        "description": "Традиционный суп",
        "ingredients": [
            {
                "name": "Картофель",
                "quantity": 1,
                "unit": "кг",
                "description": "Картофель",
            },
            {
                "name": "капуста",
                "quantity": 1,
                "unit": "кг",
                "description": "капуста",
            },
            {
                "name": "свекла",
                "quantity": 1,
                "unit": "кг",
                "description": "свекла",
            },
            {
                "name": "морковь",
                "quantity": 1,
                "unit": "кг",
                "description": "морковь",
            },
            {
                "name": "лук",
                "quantity": 1,
                "unit": "кг",
                "description": "лук",
            },
            {
                "name": "чеснок",
                "quantity": 1,
                "unit": "кг",
                "description": "чеснок",
            },
            {
                "name": "говядина",
                "quantity": 1,
                "unit": "кг",
                "description": "говядина",
            },
            {
                "name": "томатная паста",
                "quantity": 1,
                "unit": "кг",
                "description": "томатная паста",
            },
        ],
        "views": 0,
    },
    {
        "dish_name": "Пельмени",
        "cooking_time": 60,
        "description": "Лучшее блюдо - пельмени",
        "ingredients": [
            {
                "name": "Мясо",
                "quantity": 1,
                "unit": "кг",
                "description": "Мясо",
            },
            {
                "name": "лук",
                "quantity": 1,
                "unit": "кг",
                "description": "лук",
            },
            {
                "name": "мука",
                "quantity": 1,
                "unit": "кг",
                "description": "мука",
            },
            {
                "name": "вода",
                "quantity": 1,
                "unit": "кг",
                "description": "вода",
            },
            {
                "name": "соль",
                "quantity": 1,
                "unit": "кг",
                "description": "соль",
            },
        ],
        "views": 0,
    },
    {
        "dish_name": "Оливье",
        "cooking_time": 40,
        "description": "Оливье - это традиционный салат, "
        "который часто подают на праздничном столе. "
        "Все ингредиенты нарезаются на маленькие кубики"
        " и смешиваются с майонезом.",
        "ingredients": [
            {
                "name": "Картофель",
                "quantity": 1,
                "unit": "кг",
                "description": "Картофель",
            },
            {
                "name": "морковь",
                "quantity": 1,
                "unit": "кг",
                "description": "морковь",
            },
            {
                "name": "свекла",
                "quantity": 1,
                "unit": "кг",
                "description": "свекла",
            },
            {
                "name": "морковь",
                "quantity": 1,
                "unit": "кг",
                "description": "морковь",
            },
            {
                "name": "зеленый горошек",
                "quantity": 1,
                "unit": "кг",
                "description": "зеленый горошек",
            },
            {
                "name": "колбаса",
                "quantity": 1,
                "unit": "кг",
                "description": "колбаса",
            },
            {
                "name": "маринованные огурцы",
                "quantity": 1,
                "unit": "кг",
                "description": "маринованные огурцы",
            },
            {
                "name": "яйца",
                "quantity": 1,
                "unit": "кг",
                "description": "яйца",
            },
            {
                "name": "майонез",
                "quantity": 1,
                "unit": "кг",
                "description": "майонез",
            },
        ],
        "views": 0,
    },
    {
        "dish_name": "Блины",
        "cooking_time": 30,
        "description": "Блины - это тонкие русские блинчики, "
        "которые можно подавать с различными начинками, "
        "такими как варенье, сметана, икра или мясо.",
        "ingredients": [
            {
                "name": "Мука",
                "quantity": 1,
                "unit": "кг",
                "description": "Мука",
            },
            {
                "name": "молоко",
                "quantity": 1,
                "unit": "кг",
                "description": "молоко",
            },
            {
                "name": "яйца",
                "quantity": 1,
                "unit": "кг",
                "description": "яйца",
            },
            {
                "name": "сахар",
                "quantity": 1,
                "unit": "кг",
                "description": "сахар",
            },
            {
                "name": "соль",
                "quantity": 1,
                "unit": "кг",
                "description": "соль",
            },
            {
                "name": "растительное масло",
                "quantity": 1,
                "unit": "кг",
                "description": "растительное масло",
            },
        ],
        "views": 0,
    },
]

if __name__ == "__main__":
    uvicorn.run("main:app")
