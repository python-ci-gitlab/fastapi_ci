from contextlib import asynccontextmanager
from typing import AsyncGenerator

from sqlalchemy.ext.asyncio import (
    AsyncEngine,
    AsyncSession,
    async_sessionmaker,
    create_async_engine,
)
from sqlalchemy.orm import declarative_base

database_url: str = "sqlite+aiosqlite:////app/app.db"

engine: AsyncEngine = create_async_engine(database_url, echo=True)

async_session = async_sessionmaker(engine, expire_on_commit=False, class_=AsyncSession)

Base = declarative_base()


@asynccontextmanager
async def get_db():
    async with async_session() as session:
        yield session
