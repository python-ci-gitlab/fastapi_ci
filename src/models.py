from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship

from .database import Base


class Recipe(Base):
    __tablename__ = "recipe"
    id = Column(Integer, primary_key=True, index=True)
    dish_name = Column(String, index=True)
    cooking_time = Column(Integer, index=True)
    description = Column(String, index=True)
    views = Column(Integer, index=True, default=0)
    ingredients = relationship("Ingredient", back_populates="_recipe")


class Ingredient(Base):
    __tablename__ = "ingredient"
    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, index=True)
    quantity = Column(Integer, index=True)
    unit = Column(String, index=True)
    description = Column(String, index=True)
    recipe_id = Column(Integer, ForeignKey("recipe.id"))
    _recipe = relationship("Recipe", back_populates="ingredients")
