from typing import List

from pydantic import BaseModel, Field


class Ingredient(BaseModel):
    name: str = Field(..., title="Name of the ingredient")
    quantity: int = Field(..., title="Quantity of the ingredient")
    unit: str = Field(..., title="Unit of measure for the ingredient")
    description: str = Field(..., title="Description of the ingredient")


class BaseRecipe(BaseModel):
    dish_name: str = Field(..., title="Full name of the dish", min_length=3)
    cooking_time: int = Field(..., title="Cooking time in minutes", gt=2)
    description: str = Field(..., title="A beautiful description of the dish")
    ingredients: List[Ingredient] = Field(..., title="List of ingredients")


class Recipes(BaseRecipe):
    views: int = Field(0, ge=0)

    class Config:
        orm_mode = True
